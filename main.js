let boxTop = 200;
let boxLeft = 200;
// Letting box = Html Box
let box = document.getElementById("box")
// When an event key has been pressed run function e
document.addEventListener('keydown', function(e) {
// Arrow Up
if(e.key === "ArrowUp"){
    boxTop -= 10
    box.style.top = boxTop + "px";
}
// Arrow Down
else if(e.key === "ArrowDown"){
    boxTop += 10
    box.style.top = boxTop + "px";
}
// Arrow Left
else if(e.key === "ArrowLeft"){
    boxLeft -= 10
    box.style.left = boxLeft+ "px";
}
// Arrow Right
else if(e.key === "ArrowRight"){
    boxLeft += 10
    box.style.left = boxLeft+ "px";
}
})
// Included code from MDN, I don't think I need this.
// log.textContent += ` ${e.code}`;


// Included code from MDN, I don't think I need this.
// log.textContent += ` ${e.code}`;
